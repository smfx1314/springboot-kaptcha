package com.jiangfeixiang.springbootkaptcha.entity;

import lombok.Data;

/**
 * @ProjectName: springboot-kaptcha
 * @Package: com.jiangfeixiang.springbootkaptcha.entity
 * @ClassName: User
 * @Author: jiangfeixiang
 * @Description: 用户实体类
 * @Date: 2019/4/21/0021 13:50
 */
@Data
public class User {

    private Integer id;
    private String username;
    private String password;
}
