package com.jiangfeixiang.springbootkaptcha.dao;

import com.jiangfeixiang.springbootkaptcha.entity.User;
import org.apache.ibatis.annotations.Param;

/**
 * @ProjectName: springboot-kaptcha
 * @Package: com.jiangfeixiang.springbootkaptcha.dao
 * @ClassName: UserDao
 * @Author: jiangfeixiang
 * @Description:
 * @Date: 2019/4/21/0021 13:49
 */
public interface UserDao {
    User getFindByUsernameAndPassword(@Param("username")String username,@Param("password") String password);
}
