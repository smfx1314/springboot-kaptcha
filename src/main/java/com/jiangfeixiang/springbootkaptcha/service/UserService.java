package com.jiangfeixiang.springbootkaptcha.service;

import com.jiangfeixiang.springbootkaptcha.entity.User;

/**
 * @ProjectName: springboot-kaptcha
 * @Package: com.jiangfeixiang.springbootkaptcha.service
 * @ClassName: UserService
 * @Author: jiangfeixiang
 * @Description: 用户service接口
 * @Date: 2019/4/21/0021 13:55
 */
public interface UserService {

    User getFindByUsernameAndPassword(String username, String password);

}
