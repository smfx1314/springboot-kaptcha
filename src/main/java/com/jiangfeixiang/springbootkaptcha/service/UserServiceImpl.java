package com.jiangfeixiang.springbootkaptcha.service;

import com.jiangfeixiang.springbootkaptcha.dao.UserDao;
import com.jiangfeixiang.springbootkaptcha.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @ProjectName: springboot-kaptcha
 * @Package: com.jiangfeixiang.springbootkaptcha.service
 * @ClassName: UserServiceImpl
 * @Author: jiangfeixiang
 * @Description: 用户service实现类
 * @Date: 2019/4/21/0021 13:55
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    /**
     * 查询
     * @param username
     * @param password
     * @return
     */
    @Override
    public User getFindByUsernameAndPassword(String username, String password) {
        User user = userDao.getFindByUsernameAndPassword(username, password);
        if (user !=null){
            return user;
        }
        return null;
    }
}
