package com.jiangfeixiang.springbootkaptcha.controller;

import com.jiangfeixiang.springbootkaptcha.entity.User;
import com.jiangfeixiang.springbootkaptcha.service.UserService;
import com.jiangfeixiang.springbootkaptcha.utils.CodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @ProjectName: springboot-kaptcha
 * @Package: com.jiangfeixiang.springbootkaptcha.controller
 * @ClassName: UserController
 * @Author: jiangfeixiang
 * @Description: 用户controller类
 * @Date: 2019/4/21/0021 13:58
 */
@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping({"","/"})
    public String index(){

        return "login";
    }
    @RequestMapping("/getUser")
    @ResponseBody
    public Map<String,Object> getUser(String username, String password, HttpServletRequest request){
        Map<String,Object> map = new HashMap<>();
        if (!CodeUtil.checkVerifyCode(request)) {
            map.put("codefail","codefail");
            return map;
        }
        User user = userService.getFindByUsernameAndPassword(username, password);
        System.out.println("登录成功");
       if (user !=null){
           map.put("success","success");
           map.put("user",user);
           return map;
       }
        map.put("fail","用户名或者密码错误");
        return map;
    }
}
