package com.jiangfeixiang.springbootkaptcha;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.jiangfeixiang.springbootkaptcha.dao")
public class SpringbootKaptchaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootKaptchaApplication.class, args);
    }

}
